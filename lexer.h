#ifndef _LEXER_H_
#define _LEXER_H_

#include "types.h"
#include "List.h"
#include <iostream>

using namespace std;

void lex_analyzer(istream& in, List<Lexeme>& lexemes);

#endif
