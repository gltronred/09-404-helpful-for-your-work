
#ifndef _TYPES_H_
#define _TYPES_H_

#include "List.h"

struct Lexeme {
  /*
   *  type == 0  - variable
   *  type == 1  - int
   *  type == 2  - assign
   *  type == 3  - semicolon (;)
   *  type == 4  - dot (.)
   *
   *  type == 5  - plus
   *  type == 6  - mult
   */
  int type;
  char var_name[100];
  int int_value;
};

struct Command {
  char var_result[100];
  List<Lexeme> rpn;
};

struct Program {
  List<char[100]> in;
  List<Command> commands;
};

struct Var {
  char name[100];
  int value;
};

#endif
