
#include <iostream>
#include <cstring>

#include "List.h"
#include "types.h"

#include "lexer.h"
#include "syntax.h"
#include "interpreter.h"

using namespace std;

int main() {
  // Лексемы
  List<Lexeme> lexemes;
  create(lexemes);

  // Лексический анализатор, читает из cin, пишет в lexemes
  lex_analyzer(cin, lexemes);

  // Программа
  Program program;
  init_program(program);

  // Синтаксический анализатор, берёт лексемы из lexemes и пишет программу program
  syntax_analyzer(lexemes, program);

  // Переменные
  List<Var> vars;
  create(vars);

  // Var v;
  // strcpy(v.name, "abc");
  // v.value = 123;
  // add_start(vars, v);

  // Собственно интерпретатор, берёт программу, записывает значения переменных в vars
  interpret(cin, program, vars);

  // Вывод
  move_first(vars);
  while (!is_end(vars)) {
    Var var = get_cur(vars);
    cout << var.name << " = " << var.value << endl;
    
    move_next(vars);
  }

  return 0;
}

