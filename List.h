
#ifndef _LIST_H_
#define _LIST_H_

#include <cstdlib>

template <typename T>
struct Node {
	T elem;
	Node<T>* next;
};

template <typename T>
struct List {
	Node<T>* head;
	Node<T>* last;
	Node<T>* cur;
};

template <typename T>
void create(List<T>& l) {
	l.head = NULL;
	l.last = NULL;
}

template <typename T>
void add_start(List<T>& l, T elem) {
	Node<T>* p = new Node<T>;
	p->elem = elem;
	p->next = l.head;
	if (l.head == NULL)
		l.last = p;
	l.head = p;
}

template <typename T>
void add_end(List<T>& l, T elem) {
	Node<T>* p = new Node<T>;
	p->elem = elem;
	p->next = NULL;
	if (l.last != NULL)
		l.last->next = p;
	else {
		l.head = p;
	}
	l.last = p;
}

template <typename T>
void move_first(List<T>& l) {
	l.cur = l.head;
}

template <typename T>
void move_next(List<T>& l) {
	l.cur = l.cur->next;
}

template <typename T>
bool is_end(List<T> l) {
	return l.cur == NULL;
}

template <typename T>
T& get_cur(List<T> l) {
	return l.cur->elem;
}

#endif
